class KW():
    def __init__(self):
        self.__value = ""

    @classmethod
    def returnSignDict(cls):
        result = dict()
        letters = ["X", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "R", 
                   "S", "T", "U", "W", "Y", "Z"]
        for key in range(10):
            result[str(key)] = key
        for key, value in enumerate(letters):
            result[value] = key+10
        return result
    
    @classmethod
    def calculateControlSum(cls, kw):
        signArray = cls.returnSignDict()
        weights = [1,3,7]
        sum = 0
        kw = kw.replace("/", "")
        for i, j in enumerate(kw):
            if i >= 12:
                break
            sum += weights[i%3] * signArray[j]
        return sum % 10

    def setValue(self, kw):
        self.__value = kw

    def setValueWithCorrect(self, kw):
        self.setValue("{0}/{1}".format(kw[0:13], self.calculateControlSum(kw)))

    def setValueWithOldVersion(self, court, no):
        self.setValueWithCorrect(court + "/" + str(no).zfill(8))

    def getValue(self):
        return self.__value

    @classmethod
    def findSequence(cls, findSeq, judge, dest):
        output = []
        result = cls.returnListOfKW(judge, dest)
        for key,h in enumerate(result):
            pairs = str.join("", [element.getValue()[-1] for element in result[key:key+len(findSeq)]])
            seq = str.join("", [str(key) for key in findSeq])
            if pairs == seq:
                output.append(result[key])
        return output
                
    @classmethod
    def returnListOfKW(cls, judge, dest):
        result = []
        for i in range(1, dest):
            obj = cls()
            obj.setValueWithOldVersion(judge, i)
            result.append(obj)
        return result
